/**
 * @doc         https://docs.oracle.com/en/java/
 * @matkul      pemrograman 2
 * @class       X2l
 * @kelompok    3
 * @nomer       1
 *
 * */

/* Import Class Scanner */
import java.util.Scanner;

public class RataRataNilai
{
    public static void main(String[] data)
    {
        /**
         * Pertama
         *
         * Deklarasikan terlebih dahulu variable yang akan digunakan beserta
         * tipe data yang akan dipakai.
         *
         * Dan deklrasikan juga untuk variable scanner nya
         *
         * @var int     nilai1  : untuk memasukan nilai 1
         * @var int     nilai2  : untuk memasukan nilai 2
         * @var int     error   : untuk menampung angka error jika input kurang dari 0
         * @var int     rata    : untuk menampung nilai rata-rata
         * @var Scanner input   : menampung object dari class scanner
         *
         * */
        int nilai1;
        int nilai2;
        int error = 0;
        int rata;

        Scanner input = new Scanner(System.in);


        /*
        * Kedua
        *
        * Buat inputan untuk mengisi kedua nilai
        * */
        System.out.println("Program Rata-Rata Nilai Tugas");
        System.out.println("----------------------------|");

        // untuk nilai 1
        System.out.print("masukan nilai tugas 1       |= ");
        nilai1 = input.nextInt();

        System.out.println("----------------------------|");

        // untuk nilai 2
        System.out.print("masukan nilai tugas 2       |= ");
        nilai2 = input.nextInt();

        System.out.println("----------------------------|");



        /*
        * Ketiga
        *
        * Buat sebuah kondisi / pengecekan untuk memvalidasi sebuah nilai negatif / positif.
        * Nilai negatif adalah bilangan yang kurang dari < 0.
        *
        * Jika nilai1 dan nilai2 kurang dari 0 atau lebih dari 100
        * maka tambah nilai 1 kedalam variable error
        * menggunakan post increment (error++) sama saja kaya error = error + 1;
        * untuk patokan apakah nanti nya dari salah satu variable ada yang error / tidak sessuai inputannya
        *
        * */
        if(nilai1 < 0 || nilai1 > 100)
        {
            error++;
        }
        else if(nilai2 < 0 || nilai2 > 100)
        {
            error++;
        }


        /* Keempat - Output
        *
        * Jika salah satu nilai1 ataupun nilai 2 kurang dari 0 atau lebih dari 100
        * otomatis variable akan terisi 1 dengan error = 0 + 1
        *
        * maka tampilkan pesan errornya
        *
        * */
        if(error > 0)
        {
            System.out.println("Anda Menekan Angka Yang salah (nilai <0 atau nilai >100)");
        }

        /*
        * Tetapi jika tidak ada error = 0
        * maka rata-ratakan kedua nilai yang di input di atas
        * dan tampilkan pesannya
        *
        * */
        else
        {
            rata = (nilai1 + nilai2) / 2;
            System.out.println("Nilai Rata-Rata nya adalah " + rata);
        }
    }
}
